class Vehicle < ActiveRecord::Base
     
    validate :brand, presence: true
    validate :name, presence: true
    validate :year, presence: true
    validate :description, presence: true
    validate :sold, presence: true
    validate :created, presence: true
    validate :updated, presence: true
    
end